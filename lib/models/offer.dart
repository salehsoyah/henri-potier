class Offer {
  final String type;
  final int? sliceValue;
  final int value;

  const Offer({
    required this.type,
    this.sliceValue,
    required this.value,
  });

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
      type: json['type'], sliceValue: json['sliceValue'], value: json['value']);
}
