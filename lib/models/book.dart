class Book {
  final String id;
  final String title;
  final int price;
  final String cover;
  final List<dynamic> synopsis;

  const Book(
      {required this.id,
      required this.title,
      required this.price,
      required this.cover,
      required this.synopsis});

  factory Book.fromJson(Map<String, dynamic> json) => Book(
      id: json['isbn'],
      title: json['title'],
      price: json['price'],
      cover: json['cover'],
      synopsis: json['synopsis']);
}
