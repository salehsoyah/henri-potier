import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:henri_potier/models/book.dart';
import 'package:henri_potier/models/offer.dart';
import 'package:henri_potier/services/book_service.dart';

class BookProvider extends ChangeNotifier {
  List<Book> books = [];
  double totalPrice = 0;
  double bestOfferValue = 0;
  List<Offer> offers = [];
  Offer? bestOffer;

  int get bookLength {
    return books.length;
  }

  void addBook(Book book) {
    books.add(book);
    totalPrice += book.price;
    Fluttertoast.showToast(
        msg: "Livre ajouté au panier",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
    notifyListeners();
  }

  void removeBook(Book book) {
    books.remove(book);
    totalPrice -= book.price;
    Fluttertoast.showToast(
        msg: "Livre retiré du panier",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    notifyListeners();
  }

  void getBestOffer() async {
    offers = await BookService.bestOffer(books.map((e) => e.id).toList());
    bestOfferValue = totalPrice;
    for (var offer in offers) {
      if (offer.type == "percentage") {
        var percentage = totalPrice - ((totalPrice * offer.value) / 100);
        if (percentage < bestOfferValue) {
          bestOfferValue = percentage;
          bestOffer = offer;
        }
      }
      if (offer.type == "minus") {
        var minus = totalPrice - offer.value;
        if (minus < bestOfferValue) {
          bestOfferValue = minus;
          bestOffer = offer;
        }
      }
      if (offer.type == "slice") {
        var slice =
            totalPrice - ((totalPrice ~/ offer.sliceValue!) * offer.value);
        if (slice < bestOfferValue) {
          bestOfferValue = slice;
          bestOffer = offer;
        }
      }
    }
    notifyListeners();
  }
}
