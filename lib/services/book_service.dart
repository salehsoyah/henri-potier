import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:henri_potier/models/book.dart';
import 'package:henri_potier/models/offer.dart';
import 'package:http/http.dart' as http;

class BookService {
  static Future<List<Book>> getBooks(String query) async {
    String url =
        'https://us-central1-dvt-henri-potier.cloudfunctions.net/app/books';
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final List books = json.decode(response.body).toList();

      return books.map((json) => Book.fromJson(json)).where((book) {
        final titleLower = book.title.toLowerCase();
        final synopsis = book.synopsis;
        final searchLower = query.toLowerCase();
        return titleLower.contains(searchLower) ||
            synopsis
                .any((element) => element.toLowerCase().contains(searchLower));
      }).toList();
    } else {
      throw Exception();
    }
  }

  static Future<List<Offer>> bestOffer(List<String> books) async {
    List<Offer> offers = [];
    String stringList = books.join(",");
    String url =
        'https://us-central1-dvt-henri-potier.cloudfunctions.net/app/books/$stringList/commercialOffers';
    final uri = Uri.parse(url);

    try {
      final response = await http.get(
        uri,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        },
      );
      if (response.statusCode == 200) {
        final List list = json.decode(response.body)['offers'].toList();
        offers = list.map((offer) => Offer.fromJson(offer)).toList();
      } else {
        Fluttertoast.showToast(
            msg: "Pas de données",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } catch (e) {}
    return offers;
  }
}
