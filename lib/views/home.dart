import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:henri_potier/models/book.dart';
import 'package:henri_potier/provider/book_provider.dart';
import 'package:henri_potier/services/book_service.dart';
import 'package:henri_potier/views/basket.dart';
import 'package:henri_potier/views/book_details.dart';
import 'package:henri_potier/views/widgets/book_item.dart';
import 'package:henri_potier/views/widgets/search.dart';
import 'package:henri_potier/views/widgets/shimmer.dart';
import 'package:provider/provider.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Book> books = [];
  String query = '';
  Timer? debouncer;

  Future init() async {
    final books = await BookService.getBooks(query);

    setState(() => this.books = books);
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }
    debouncer = Timer(duration, callback);
  }

  Future searchBook(String query) async => debounce(() async {
        final books = await BookService.getBooks(query);

        if (!mounted) return;

        setState(() {
          this.query = query;
          this.books = books;
        });
      });

  @override
  void initState() {
    init();

    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text("La bibliothèque d'Henri Potier",
                  style: GoogleFonts.roboto(
                      fontWeight: FontWeight.bold, fontSize: 30)),
              const SizedBox(
                height: 20,
              ),
              buildSearch(),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                  child: books.isNotEmpty
                      ? ListView.builder(
                          shrinkWrap: true,
                          padding: const EdgeInsets.fromLTRB(10.0, 10.0, 0, 0),
                          itemCount: books.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 25.0),
                              child: InkWell(
                                  onTap: () {},
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => BookDetails(
                                                  book: books[index])),
                                        );
                                      },
                                      child: BookItem(
                                        action: true,
                                        book: books[index],
                                      ))),
                            );
                          })
                      : const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: ShimmerWidget())),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Recherche',
        onChanged: searchBook,
      );
}
