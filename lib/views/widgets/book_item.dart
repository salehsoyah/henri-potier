import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:henri_potier/models/book.dart';
import 'package:henri_potier/provider/book_provider.dart';
import 'package:provider/provider.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

class BookItem extends StatefulWidget {
  BookItem({Key? key, required this.book, required this.action})
      : super(key: key);
  bool action;
  Book book;

  @override
  State<BookItem> createState() => _BookItemState();
}

class _BookItemState extends State<BookItem> {
  @override
  Widget build(BuildContext context) {
    var bookProvider = Provider.of<BookProvider>(context, listen: false);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 3,
          child: Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                ),
              ],
            ),
            child: Image.network(
              widget.book.cover,
              fit: BoxFit.fill,
              loadingBuilder: (BuildContext context, Widget child,
                  ImageChunkEvent? loadingProgress) {
                if (loadingProgress == null) return child;
                return Shimmer(
                  direction: const ShimmerDirection.fromLTRB(),
                  color: const Color.fromRGBO(38, 38, 38, 0.4),
                  colorOpacity: 0,
                  child: Container(
                    width: 70.0,
                    height: 130.0,
                    color: Colors.white,
                  ),
                );
              },
            ),
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Expanded(
          flex: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.book.title,
                style: GoogleFonts.roboto(
                    fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ],
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  onPressed: () {
                    if (widget.action) {
                      bookProvider.addBook(widget.book);
                      bookProvider.getBestOffer();
                    } else {
                      bookProvider.removeBook(widget.book);
                      bookProvider.getBestOffer();
                    }
                  },
                  icon: Icon(
                    widget.action
                        ? Icons.add_shopping_cart
                        : Icons.remove_shopping_cart,
                    size: 30,
                    color: Colors.grey,
                  )),
              const SizedBox(
                height: 10,
              ),
              Text(
                '${widget.book.price}€',
                style: GoogleFonts.roboto(
                    fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
