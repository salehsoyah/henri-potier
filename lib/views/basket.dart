import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:henri_potier/models/offer.dart';
import 'package:henri_potier/provider/book_provider.dart';
import 'package:henri_potier/views/book_details.dart';
import 'package:henri_potier/views/widgets/book_item.dart';
import 'package:provider/provider.dart';

class Basket extends StatefulWidget {
  const Basket({Key? key}) : super(key: key);

  @override
  State<Basket> createState() => _BasketState();
}

class _BasketState extends State<Basket> {
  @override
  Widget build(BuildContext context) {
    var bookProvider = Provider.of<BookProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Icon(
                            Icons.shopping_cart,
                            size: 20,
                            color: Colors.white,
                          ),
                          Text(
                            '${bookProvider.bookLength} livres',
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              (bookProvider.bestOffer != null && bookProvider.books.isNotEmpty)
                  ? Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: Colors.blue.withOpacity(0.3), width: 2)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Text("Meileure promotion:",
                                      style: GoogleFonts.roboto(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20)),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          "Type: ${bookProvider.bestOffer!.type}",
                                          style: GoogleFonts.roboto(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17)),
                                      bookProvider.bestOffer!.sliceValue != null
                                          ? Text(
                                              "Valeur de tranche: ${bookProvider.bestOffer!.sliceValue}€",
                                              style: GoogleFonts.roboto(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 17))
                                          : Container(),
                                      Text(
                                          "Valeur: ${bookProvider.bestOffer!.value}€",
                                          style: GoogleFonts.roboto(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17)),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Text("Prix total: ${bookProvider.totalPrice}€",
                                style: GoogleFonts.roboto(
                                    fontWeight: FontWeight.bold, fontSize: 20)),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                                "Prix total aprés la promotion: ${bookProvider.bestOfferValue}€",
                                style: GoogleFonts.roboto(
                                    fontWeight: FontWeight.bold, fontSize: 20)),
                          ],
                        ),
                      ),
                    )
                  : Container(),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                  child: bookProvider.books.isNotEmpty
                      ? ListView.builder(
                          shrinkWrap: true,
                          padding: const EdgeInsets.fromLTRB(10.0, 10.0, 0, 0),
                          itemCount: bookProvider.books.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 25.0),
                              child: InkWell(
                                  onTap: () {},
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => BookDetails(
                                                  book: bookProvider
                                                      .books[index])),
                                        );
                                      },
                                      child: BookItem(
                                        action: false,
                                        book: bookProvider.books[index],
                                      ))),
                            );
                          })
                      : Center(
                          child: Text(
                            "Panier vide",
                            style: GoogleFonts.roboto(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        )),
            ],
          ),
        ),
      ),
    );
  }
}
