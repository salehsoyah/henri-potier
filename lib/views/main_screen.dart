import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:henri_potier/provider/book_provider.dart';
import 'package:henri_potier/views/basket.dart';
import 'package:henri_potier/views/home.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int pageIndex = 0;

  final pages = [const Home(), const Basket()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[pageIndex],
      bottomNavigationBar: buildMyNavBar(context),
    );
  }

  Container buildMyNavBar(BuildContext context) {
    var bookProvider = Provider.of<BookProvider>(context);

    return Container(
      height: 60,
      decoration: BoxDecoration(
        color: Colors.blue.withOpacity(0.5),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            enableFeedback: false,
            onPressed: () {
              setState(() {
                pageIndex = 0;
              });
            },
            icon: pageIndex == 0
                ? const Icon(
                    Icons.home_filled,
                    color: Colors.white,
                    size: 35,
                  )
                : const Icon(
                    Icons.home_outlined,
                    color: Colors.white,
                    size: 35,
                  ),
          ),
          Row(
            children: [
              IconButton(
                enableFeedback: false,
                onPressed: () {
                  setState(() {
                    pageIndex = 1;
                  });
                },
                icon: pageIndex == 1
                    ? const Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                        size: 35,
                      )
                    : const Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.white,
                        size: 35,
                      ),
              ),
              bookProvider.bookLength > 0
                  ? Text('(${bookProvider.bookLength})',
                      style: GoogleFonts.roboto(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.white))
                  : Container()
            ],
          ),
        ],
      ),
    );
  }
}
